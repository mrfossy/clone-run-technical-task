﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ContinueMenu : MonoBehaviour
{
    [SerializeField] MenuControl menuControlRef_;

    [SerializeField] float continueTime = 5.0f;
    float continueTimer = 0.0f;

    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] Image timerImage;

    bool buttonPressed = false;

    bool isActive = false;

    public int continueScore = 0;
    public float continueSpeed = 0f;

    public void ResetEndGamePanel(int score, float speed)
    {
        continueScore = score;
        continueSpeed = speed;

        timerText.text = Mathf.CeilToInt(continueTimer).ToString("N0");
        timerImage.fillClockwise = !timerImage.fillClockwise;

        buttonPressed = false;

        isActive = true;

        continueTimer = continueTime;
    }

    void LateUpdate()
    {
        if (!isActive) return;

        continueTimer -= Time.deltaTime;
        timerText.text = Mathf.CeilToInt(continueTimer).ToString("N0");

        timerImage.fillAmount = continueTimer / continueTime;

        if (continueTimer <= 0f)
        {
            HandleNoButtonPressed();
        }
    }

    public void HandleNoButtonPressed()
    {
        if (buttonPressed) return;
        buttonPressed = true;

        menuControlRef_.SetShowMenu(true);

        isActive = false;
    }

    public void HandleYesButtonPressed()
    {
        if (buttonPressed) return;
        buttonPressed = true;

        menuControlRef_.ContinueGame();

        isActive = false;
    }
}
